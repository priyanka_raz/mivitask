package com.mivi.mivitask.common.utils;

import android.app.Application;
import android.content.Context;

import com.mivi.mivitask.common.data.app_preferences.AppPreferences;

/**
 * Created by priyanka on 31/3/18.
 */

public class MiviTaskApplication extends Application {

    private static Context mContextApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        AppPreferences.initPreferences(this);
        mContextApplication = getApplicationContext();
    }

    /* retrieve application context
     * @return Context
     */
    public static Context getAppContext() {
        return mContextApplication;
    }
}

