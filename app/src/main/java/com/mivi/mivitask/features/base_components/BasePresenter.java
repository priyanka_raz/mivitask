package com.mivi.mivitask.features.base_components;


import com.google.gson.Gson;
import com.mivi.mivitask.common.data.app_preferences.AppPreferences;

/**
 * Created by priyanka on 31/3/18.
 */

public class BasePresenter<V extends MvpView>
        implements MvpPresenter<V> {

    private V mMvpView;
    private AppPreferences appPreferences;
    private Gson gson;

    public BasePresenter() {

    }

    public BasePresenter(AppPreferences appPreferences) {
        this.appPreferences = appPreferences;
    }

    public BasePresenter(AppPreferences appPreferences, Gson gson) {
        this.appPreferences = appPreferences;
        this.gson = gson;
    }

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public Gson getGson() {
        return gson;
    }

    public AppPreferences getAppPreferences() {
        return appPreferences;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {

        public MvpViewNotAttachedException() {
            super("Please call Presenter.onAttach(MvpView) before" +
                    " requesting data to the Presenter");
        }

    }


}
