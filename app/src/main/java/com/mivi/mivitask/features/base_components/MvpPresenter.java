package com.mivi.mivitask.features.base_components;

/**
 * Created by priyanka on 31/3/18.
 */

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V mvpView);

    void onDetach();
}
