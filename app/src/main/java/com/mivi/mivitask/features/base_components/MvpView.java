package com.mivi.mivitask.features.base_components;

import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Created by priyanka on 31/3/18.
 */

public interface MvpView {

    void showLoading();

    void hideLoading();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    boolean isNetworkConnected();

    void hideKeyboard();

    void showActionSnackBar(String message);

    Context getActivity();
}
