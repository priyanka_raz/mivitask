package com.mivi.mivitask.features.launch;

import android.content.Intent;
import android.os.Bundle;

import com.mivi.mivitask.R;
import com.mivi.mivitask.features.base_components.BaseActivity;
import com.mivi.mivitask.features.login.LoginActivity;

public class SplashActivity extends BaseActivity implements SplashMvpView {

    private SplashPresenter<SplashMvpView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mPresenter = new SplashPresenter<>();
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void setUp() {
        mPresenter.addDummyDelay();
    }

    @Override
    public void navigateToNextScreen() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }
}
