package com.mivi.mivitask.features.launch;

import com.mivi.mivitask.features.base_components.MvpPresenter;

/**
 * Created by priyanka on 31/3/18.
 */

public interface SplashMvpPresenter<V extends SplashMvpView> extends MvpPresenter<V> {

    void addDummyDelay();
}
