package com.mivi.mivitask.features.launch;

import com.mivi.mivitask.features.base_components.MvpView;

/**
 * Created by priyanka on 31/3/18.
 */

public interface SplashMvpView extends MvpView {

    void navigateToNextScreen();
}

