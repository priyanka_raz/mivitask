package com.mivi.mivitask.features.launch;

import android.os.Handler;
import android.os.Looper;

import com.mivi.mivitask.features.base_components.BasePresenter;

/**
 * Created by priyanka on 31/3/18.
 */

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V> implements SplashMvpPresenter<V> {

    public SplashPresenter() {
        super();
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }


    @Override
    public void addDummyDelay() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                getMvpView().navigateToNextScreen();
            }
        }, 1500);
    }
}
