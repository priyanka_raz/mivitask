package com.mivi.mivitask.features.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mivi.mivitask.R;
import com.mivi.mivitask.common.utils.CommonUtils;
import com.mivi.mivitask.features.base_components.BaseActivity;

public class LoginActivity extends BaseActivity implements LoginMvpView, View.OnClickListener {

    private RelativeLayout loginRL;
    private EditText mobileEditText, emailEditText;

    private LoginPresenter<LoginMvpView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mPresenter = new LoginPresenter<>();
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void setUp() {
        mobileEditText = (EditText) findViewById(R.id.phone_number_editText);
        emailEditText = (EditText) findViewById(R.id.email_editText);
        loginRL = (RelativeLayout) findViewById(R.id.login_button_rl);
        loginRL.setOnClickListener(this);

        mobileEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginRL.performClick();
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button_rl:
                //action
                checkValidation();
                break;
        }
    }

    private void checkValidation() {
        if (CommonUtils.isBlankField(emailEditText)) {
            showMessage("Please enter Email Id");
        } else if (CommonUtils.isValidEmail(emailEditText.getText().toString())) {
            showMessage("Please enter valid  Email Id");
        } else if (CommonUtils.isBlankField(mobileEditText)) {
            showMessage("Please enter mobile number");
        } else if (CommonUtils.textField(mobileEditText).length() != 10) {
            showMessage("Please enter valid mobile number");
        } else {
//            mPresenter.loginUser(CommonUtils.textField(mobileEditText), CommonUtils.textField(emailEditText));

        }
    }

    @Override
    public void navigateToNextScreen() {
        //save login true

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }
}
