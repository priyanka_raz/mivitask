package com.mivi.mivitask.features.login;

import com.mivi.mivitask.features.base_components.MvpPresenter;

/**
 * Created by priyanka on 31/3/18.
 */

public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {

    void loginUser();
}
