package com.mivi.mivitask.features.login;

import com.mivi.mivitask.features.base_components.MvpView;

/**
 * Created by priyanka on 31/3/18.
 */

public interface LoginMvpView extends MvpView {

    void navigateToNextScreen();
}

