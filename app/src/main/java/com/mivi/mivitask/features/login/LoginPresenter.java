package com.mivi.mivitask.features.login;

import com.mivi.mivitask.features.base_components.BasePresenter;

/**
 * Created by priyanka on 31/3/18.
 */

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {

    public LoginPresenter() {
        super();
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);
    }

    @Override
    public void loginUser() {

    }
}
